<?php

namespace BinaryStudioAcademy\Task1;

class Calculator
{
    /**
     * @param \int[] ...$numbers
     * @return int
     */
    public function add(int ...$numbers) : int
    {
        return array_sum($numbers);
    }

    /**
     * @param \int[] ...$numbers
     * @return int
     */
    public function subtract(int ...$numbers) : int
    {
        return array_subtract($numbers);
    }

    /**
     * @param \int[] ...$numbers
     * @return int
     */
    public function multiply(int ...$numbers) : int
    {
        return array_multiply($numbers);
    }

    /**
     * @param \int[] ...$numbers
     * @return int
     */
    public function divide(int ...$numbers) : int
    {
        return array_divide($numbers);
    }

    /**
     * @param int $exponent
     * @return int
     */
    public function pow2(int $exponent) : int
    {
        return pow(2, $exponent);
    }
}
