<?php

namespace BinaryStudioAcademy\Task2;

class UsersPresenter
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * UsersPresenter constructor.
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function getOrderedByLastName(): array
    {
        $users = $this->repository->getAll();

        usort($users, function ($a, $b) {
            return $a['last_name'] <=> $b['last_name'];
        });

        return $users;
    }
}
