<?php

if (! function_exists('array_subtract')) {
    /**
     * @param $numbers
     * @return mixed
     */
    function array_subtract($numbers)
    {
        $result = reset($numbers);

        foreach (array_slice($numbers, 1) as $number) {
            $result -= $number;
        }

        return $result;
    }
}

if (! function_exists('array_multiply')) {
    /**
     * @param $numbers
     * @return mixed
     */
    function array_multiply($numbers)
    {
        $result = reset($numbers);

        foreach (array_slice($numbers, 1) as $number) {
            $result *= $number;
        }

        return $result;
    }
}

if (! function_exists('array_divide')) {
    /**
     * @param $numbers
     * @return mixed
     * @throws DivisionByZeroError
     */
    function array_divide($numbers)
    {
        $result = reset($numbers);

        foreach (array_slice($numbers, 1) as $number) {
            $result = intdiv($result, $number);
        }

        return $result;
    }
}
